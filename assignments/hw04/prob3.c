
#include <stdio.h>

int main {

	int a = 3; // addi $s0, $0, 3
	int b = 1; // addi $s1, $0, 1
	int c;
	c = a + b; // add  $t0, $s0, $s1

	while (c != 0) // loop
		c--;

	// loop:
	// beq $t0, $0, sequel
	// nop
	// addi $t0, $t0, -1
	// j loop
	// nop

}
